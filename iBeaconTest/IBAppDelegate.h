//
//  IBAppDelegate.h
//  iBeaconTest
//
//  Created by Nicholas Wilkerson on 9/28/13.
//
//

#import <UIKit/UIKit.h>

@interface IBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
