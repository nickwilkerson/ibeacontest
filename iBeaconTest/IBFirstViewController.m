//
//  IBFirstViewController.m
//  iBeaconTest
//
//  Created by Nicholas Wilkerson on 9/28/13.
//
//

#import "IBFirstViewController.h"

@interface IBFirstViewController ()

@end

@implementation IBFirstViewController
@synthesize tableView=_tableView, beaconsArray=_beaconsArray;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super init];
    if (self) {
        
        
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.beaconsArray = [[NSMutableArray alloc] initWithObjects:@"Start Monitoring to Search for Beacons", nil];
    locationManager = [[CLLocationManager alloc] init];
    [locationManager setDelegate:self];

    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
    monitoring = NO;



    //[locationManager startRangingBeaconsInRegion:beaconRegion];

}

-(IBAction)startStopMonitoring:(id)sender {
    CLBeaconRegion *beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:@"506C5C8A-A44A-4CAA-AC08-3DE2517E48F0"] identifier:@"com.nsnick.iBeaconTest"];
    if (monitoring) {
        [locationManager stopRangingBeaconsInRegion:beaconRegion];
        [locationManager stopMonitoringForRegion:beaconRegion];
        [startStopMonitoringButton setTitle:@"Start Monitoring" forState:UIControlStateNormal];
        monitoring = NO;
    } else {
        [locationManager startMonitoringForRegion:beaconRegion];
        [startStopMonitoringButton setTitle:@"Stop Monitoring" forState:UIControlStateNormal];
        [locationManager startRangingBeaconsInRegion:beaconRegion];
        monitoring = YES;
    }
}

#pragma CLLocationManageDelegate methods

-(void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    NSLog(@"locationManager: didEnterRegeion:");
}

-(void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    NSLog(@"locationManager: didExitRegion:");
}

-(void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region {
    NSString *stateString;
    if (state == CLRegionStateUnknown) {
        stateString = @"CLRegionStateUnknown";
    } else if (state == CLRegionStateOutside) {
        stateString = @"CLRegionStateOutside";
    } else if (state == CLRegionStateInside){
        stateString = @"CLRegionStateInside";
    } else {
        stateString = @"error";
    }
    NSLog(@"locationManager: didDetermineState:%@ forRegion:%@",stateString,region.identifier);
}

-(void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error{
    NSLog(@"locationManager: monitoringDidFailForRegion:");
}

-(void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region {
    NSLog(@"locationManager: didStartMonitoringForRegion:");
}

-(void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region {
    NSLog(@"locationManager: didRangeBeacons:%@ inRegion:", beacons);
   // NSLog(@"number of beacons:%d", [beacons count]);
    [self.beaconsArray removeAllObjects];
    
    [self.beaconsArray addObjectsFromArray:beacons];

    [self.tableView reloadData];
    
    //[self.tableView setNeedsDisplay];
}

-(void)locationManager:(CLLocationManager *)manager rangingBeaconsDidFailForRegion:(CLBeaconRegion *)region withError:(NSError *)error {
    NSLog(@"locationManager: rangingBeaconsDidFailForRegion: withError:\"%@\"", [error localizedDescription]);
}

#pragma TableViewDataSource methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSLog(@"numberOfSectionsInTableView");
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"numberOfRowsInSection %d", [self.beaconsArray count]);
    return [self.beaconsArray count];
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"cellForRowAtIndexPath");
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.textColor = [UIColor blackColor];
    if ([[self.beaconsArray objectAtIndex:indexPath.row] isKindOfClass:[CLBeacon class]]) {
        UIFont *font;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            font = [ UIFont fontWithName: @"Arial" size: 10.0 ];
        } else {
            font = [ UIFont fontWithName: @"Arial" size: 13.0 ];
        }
        cell.textLabel.font  = font;
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.text = [NSString stringWithFormat:@"%@ proximity:%d +/- %fm rssi:%d",[[_beaconsArray objectAtIndex:indexPath.row ] proximityUUID],[[_beaconsArray objectAtIndex:indexPath.row ] proximity], [[_beaconsArray objectAtIndex:indexPath.row ] accuracy], [[_beaconsArray objectAtIndex:indexPath.row ] rssi]];
    } else {
        cell.textLabel.text = [self.beaconsArray objectAtIndex:indexPath.row];
    }
    return cell;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
