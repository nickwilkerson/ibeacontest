//
//  IBSecondViewController.m
//  iBeaconTest
//
//  Created by Nicholas Wilkerson on 9/28/13.
//
//

#import "IBSecondViewController.h"


@interface IBSecondViewController ()

@end

@implementation IBSecondViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    CLBeaconRegion *beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:@"506C5C8A-A44A-4CAA-AC08-3DE2517E48F0"] identifier:@"com.nsnick.iBeaconTest"];
	beaconPeripheralData = [beaconRegion peripheralDataWithMeasuredPower:nil];
    // Create the peripheral manager.
    peripheralManager = [[CBPeripheralManager alloc] initWithDelegate:self queue:nil options:nil];
    
    // Start advertising your beacon's data.
    
    
}

-(IBAction)startAdvertising:(id)sender{
    if (advertising) {
        [peripheralManager stopAdvertising];
        [startStopAdvertisingButton setTitle:@"Start Advertising" forState:UIControlStateNormal];
        advertising = NO;
    } else {
        [peripheralManager startAdvertising:beaconPeripheralData];
        advertising = YES;
        [startStopAdvertisingButton setTitle:@"Stop Advertising" forState:UIControlStateNormal];
    }
    
}

-(void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral {
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
