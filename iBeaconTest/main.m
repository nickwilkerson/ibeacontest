//
//  main.m
//  iBeaconTest
//
//  Created by Nicholas Wilkerson on 9/28/13.
//
//

#import <UIKit/UIKit.h>

#import "IBAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IBAppDelegate class]));
    }
}
