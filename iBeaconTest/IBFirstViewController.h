//
//  IBFirstViewController.h
//  iBeaconTest
//
//  Created by Nicholas Wilkerson on 9/28/13.
//
//
#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocationManager.h>
#import <CoreLocation/CLBeaconRegion.h>

#import <UIKit/UIKit.h>

@interface IBFirstViewController : UIViewController <CLLocationManagerDelegate, UITableViewDataSource, UITableViewDelegate> {
    BOOL monitoring;
    CLLocationManager *locationManager;
    IBOutlet UIButton *startStopMonitoringButton;
    IBOutlet UITableView *_tableView;
    NSMutableArray *_beaconsArray;
}

@property (nonatomic, retain)UITableView *tableView;
@property (nonatomic, retain)NSMutableArray *beaconsArray;

@end
