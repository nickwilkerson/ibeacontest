//
//  IBSecondViewController.h
//  iBeaconTest
//
//  Created by Nicholas Wilkerson on 9/28/13.
//
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface IBSecondViewController : UIViewController <CBPeripheralManagerDelegate> {
    BOOL advertising;
    CBPeripheralManager *peripheralManager;
    NSDictionary *beaconPeripheralData;
    IBOutlet UIButton *startStopAdvertisingButton;
}

-(IBAction)startAdvertising:(id)sender;

@end
